package types

import (
	"errors"
	"time"
)

var ErrTooLong = errors.New("the request takes too long, please specify more precise filters")

type LogsInput struct {
	Level            string     `query:"level" json:"level"`
	Message          string     `query:"message" json:"message"`
	ResourceID       string     `query:"resourceId" json:"resourceId"`
	StartDate        *time.Time `query:"startDate" json:"startDate"`
	EndDate          *time.Time `query:"endDate" json:"endDate"`
	TraceID          string     `query:"traceId" json:"traceId"`
	SpanID           string     `query:"spanId" json:"spanId"`
	Commit           string     `query:"commit" json:"commit"`
	ParentResourceID string     `query:"parentResourceId" json:"parentResourceId"`

	Offset int64 `query:"offset" json:"offset"`
	Limit  int64 `query:"limit" json:"limit"`
}

type LogCreateInput struct {
	Level      string    `json:"level" bson:"level" validate:"required"`
	Message    string    `json:"message" bson:"message" validate:"required"`
	ResourceID string    `json:"resourceId" bson:"resourceId" validate:"required"`
	Timestamp  time.Time `json:"timestamp" bson:"timestamp" validate:"required"`
	TraceID    string    `json:"traceId" bson:"traceId" validate:"required"`
	SpanID     string    `json:"spanId" bson:"spanId" validate:"required"`
	Commit     string    `json:"commit" bson:"commit" validate:"required"`
	Metadata   Metadata  `json:"metadata" bson:"metadata" validate:"required,dive,required"`
}

type Log struct {
	ID         string    `json:"id" bson:"_id"`
	Level      string    `json:"level" bson:"level"`
	Message    string    `json:"message" bson:"message"`
	ResourceID string    `json:"resourceId" bson:"resourceId"`
	Timestamp  time.Time `json:"timestamp" bson:"timestamp"`
	TraceID    string    `json:"traceId" bson:"traceId"`
	SpanID     string    `json:"spanId" bson:"spanId"`
	Commit     string    `json:"commit" bson:"commit"`
	Metadata   Metadata  `json:"metadata" bson:"metadata"`
}

type Metadata struct {
	ParentResourceID string `json:"parentResourceId" bson:"parentResourceId"`
}

type Logs []Log
