import path from 'node:path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';

const pathSrc = path.resolve(__dirname, 'src');

export default defineConfig({
  base: './',
  build: {
    outDir: '../cmd/ingestor/dist',
  },
  resolve: {
    alias: {
      '@/': `${pathSrc}/`,
      '@stores/': `${pathSrc}/stores/`,
      '@customTypes/': `${pathSrc}/types/`,
      '@components/': `${pathSrc}/components/`,
      '@composables/': `${pathSrc}/composables/`,
    },
  },
  server: {
    host: true,
    proxy: {
      '^/api/v1/*': {
        target: 'http://localhost:3000',
        changeOrigin: true,
      },
    },
  },
  plugins: [
    vue(),
    Components({
      extensions: ['vue'],
      include: [/\.vue$/, /\.vue?vue/],
      resolvers: [ElementPlusResolver()],
      dts: path.resolve(pathSrc, 'components.d.ts'),
    }),
    AutoImport({
      imports: ['vue'],
      resolvers: [ElementPlusResolver({ importStyle: false })],
      dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
    }),
  ],
  optimizeDeps: {
    include: ['vue', '@vueuse/core'],
  },
});
