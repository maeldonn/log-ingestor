export interface LogInput {
  level?: string;
  message?: string;
  resourceId?: string;
  startDate?: string;
  endDate?: string;
  traceId?: string;
  spanId?: string;
  commit?: string;
  parentResourceId?: string;

  autoRefresh: boolean;
  refreshInterval: number;
}

export interface Log {
  id: string;
  level: string;
  message: string;
  resourceId: string;
  timestamp: string;
  traceId: string;
  spanId: string;
  commit: string;
  metadata: { parentResourceId: string };
}

export enum LogLevel {
  INFO = 'info',
  ERROR = 'error',
  WARNING = 'warning',
  DEBUG = 'debug',
  FATAL = 'fatal',
}
