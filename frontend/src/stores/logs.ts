import { ref } from 'vue';
import { Log, LogInput } from '@customTypes/log';
import { ElMessage } from 'element-plus';
import { useLocalStorage } from '@vueuse/core';

const logs = ref([] as Log[]);
const loading = ref(false);
const filters = useLocalStorage('filters', <LogInput>{
  level: 'info',
  refreshInterval: 1,
});

export const useLogs = () => {
  const getLogs = async () => {
    if (loading.value) return;
    loading.value = true;

    try {
      const params = getParams();
      const response = await fetch(
        new URL(`http://localhost:3000/api/v1?limit=100&${params}`),
      );
      const json = await response.json();
      if (response.ok) {
        logs.value = json;
      } else {
        throw new Error(json.message);
      }
    } catch (error) {
      ElMessage.error(`${error}`);
    } finally {
      loading.value = false;
    }
  };

  const getParams = () => {
    const params = new URLSearchParams({});
    for (const [key, value] of Object.entries(filters.value)) {
      if (!value) continue;
      if (['startDate', 'endDate'].includes(key)) {
        params.set(key, new Date(value).toISOString());
      } else {
        params.set(key, value);
      }
    }
    return params;
  };

  onMounted(() => getLogs());

  return {
    logs,
    filters,
    loading,
    getLogs,
  };
};
