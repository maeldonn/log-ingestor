package handlers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/maeldonn/ingestor/database"
	"gitlab.com/maeldonn/ingestor/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// LogHandler is the log handler
type LogHandler struct {
	MongoDB *mongo.Client
}

// NewLog create a LogHandler
func NewLog(mongoDB *mongo.Client) *LogHandler {
	return &LogHandler{
		MongoDB: mongoDB,
	}
}

// Ping response to HEAD request
func (LogHandler) Ping(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}

// FindLogs return logs according to input filters
func (lh LogHandler) FindLogs(c echo.Context) error {
	ctx, cancel := context.WithTimeout(c.Request().Context(), 1*time.Second)
	defer cancel()
	c.SetRequest(c.Request().WithContext(ctx))

	defer func() {
		if err := ctx.Err(); err != nil {
			c.Logger().Error(fmt.Errorf("find request is taking too long: %w", err))
			c.Error(echo.NewHTTPError(http.StatusForbidden, types.ErrTooLong))
		}
	}()

	var in types.LogsInput
	if err := c.Bind(&in); err != nil {
		c.Logger().Error(fmt.Errorf("failed to bind and validate search input: %w", err))
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	collection := database.LogsCollection(lh.MongoDB)
	cursor, err := collection.Find(c.Request().Context(), makeFilter(in), makeOptions(in))
	if err != nil {
		c.Logger().Error(fmt.Errorf("failed to find mongoDB documents: %w", err))
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	defer cursor.Close(c.Request().Context())

	logs := types.Logs{}
	if err := cursor.All(c.Request().Context(), &logs); err != nil {
		c.Logger().Error(fmt.Errorf("failed to decode mongoDB documents: %w", err))
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, logs)
}

// makeFilter return a BSON document corresponding to input filters
func makeFilter(in types.LogsInput) bson.D {
	filter := bson.D{}

	if in.Level != "" {
		filter = append(filter, bson.E{Key: "level", Value: in.Level})
	}

	if in.Message != "" {
		filter = append(filter, bson.E{
			Key:   "message",
			Value: primitive.Regex{Pattern: in.Message, Options: "i"},
		})
	}

	if in.ResourceID != "" {
		filter = append(filter, bson.E{
			Key:   "resourceId",
			Value: primitive.Regex{Pattern: in.ResourceID, Options: "i"},
		})
	}

	if in.StartDate != nil {
		filter = append(filter, bson.E{
			Key: "timestamp",
			Value: bson.M{
				"$gte": in.StartDate,
			},
		})
	}

	if in.EndDate != nil {
		filter = append(filter, bson.E{
			Key: "timestamp",
			Value: bson.M{
				"$lte": in.EndDate,
			},
		})
	}

	if in.TraceID != "" {
		filter = append(filter, bson.E{
			Key:   "traceId",
			Value: primitive.Regex{Pattern: in.TraceID, Options: "i"},
		})
	}

	if in.SpanID != "" {
		filter = append(filter, bson.E{
			Key:   "spanId",
			Value: primitive.Regex{Pattern: in.SpanID, Options: "i"},
		})
	}

	if in.Commit != "" {
		filter = append(filter, bson.E{
			Key:   "commit",
			Value: primitive.Regex{Pattern: in.Commit, Options: "i"},
		})
	}

	if in.ParentResourceID != "" {
		filter = append(filter, bson.E{
			Key:   "metadata.parentResourceId",
			Value: primitive.Regex{Pattern: in.ParentResourceID, Options: "i"},
		})
	}

	return filter
}

// makeOptions return find options
func makeOptions(in types.LogsInput) *options.FindOptions {
	opts := options.Find()
	opts.SetSort(bson.D{{Key: "timestamp", Value: -1}})

	if in.Offset != 0 {
		opts.SetSkip(in.Offset)
	}

	if in.Limit != 0 {
		opts.SetLimit(in.Limit)
	}

	return opts
}

// AddLog create a log
func (lh LogHandler) AddLog(c echo.Context) error {
	var log types.LogCreateInput
	if err := c.Bind(&log); err != nil {
		c.Logger().Error(fmt.Errorf("failed to bind and validate create input: %w", err))
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	collection := database.LogsCollection(lh.MongoDB)
	if _, err := collection.InsertOne(c.Request().Context(), log); err != nil {
		c.Logger().Error(fmt.Errorf("failed to insert a log in mongoDB: %w", err))
		return fmt.Errorf("failed to insert log: %w", err)
	}

	return c.NoContent(http.StatusCreated)
}
