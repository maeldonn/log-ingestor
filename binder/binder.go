package binder

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
)

// ValidateBinder is a custom binder
type ValidateBinder struct {
	binder    echo.DefaultBinder
	validator *validator.Validate
}

// New create a ValidateBinder
func New() *ValidateBinder {
	return &ValidateBinder{
		validator: validator.New(),
	}
}

// Bind implement the echo.Binder interface
func (vb ValidateBinder) Bind(i interface{}, c echo.Context) error {
	if err := vb.binder.Bind(i, c); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	if err := vb.validator.Struct(i); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return nil
}
