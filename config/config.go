package config

import (
	"fmt"
	"os"
	"strconv"

	"github.com/go-playground/validator"
)

// Config is the application configuration
type Config struct {
	Port     int
	MongoURI string `validate:"required"`
	Debug    bool
}

// Load load configuration from environement variables
func Load() (*Config, error) {
	cfg := Config{
		Port:     3000,
		MongoURI: os.Getenv("MONGO_URI"),
	}

	if debug, err := strconv.ParseBool(os.Getenv("DEBUG")); err == nil {
		cfg.Debug = debug
	}

	if port, err := strconv.Atoi(os.Getenv("PORT")); err == nil {
		cfg.Port = port
	}

	if err := validator.New().Struct(cfg); err != nil {
		return nil, fmt.Errorf("failed to validate config: %w", err)
	}

	return &cfg, nil
}
