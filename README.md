# Log ingestor

_(A log ingestor with a simple interface)_

![image](docs/demo.png)

## How to run

You will need docker to launch project

```bash
$ make run
```

## How to dev

If first launch:
```bash
$ make build
```

Launch backend and database:
```bash
$ make dev
```

Launch frontend:
```bash
$ make frontend
```

## How to generate logs

To generate a data sample:
```bash
$ make populate
```

> You need to have backend launched first

To launch a realtime log generator:
```bash
$ make generator
```

> When using `make run`, generator is already launched

## Configuration

### Log ingestor

- PORT:
description=port of the application
default=3000

- MONGO_URI:
description=url of the mongo database
default=mongodb://localhost:27017/logs

### Log generator

- LOG_INGESTOR_URL:
description=url of the log ingestor used for log creation
default=http://localhost:3000/api/v1

### Populate script

- LOG_INGESTOR_URL:
description=url of the log ingestor used for log creation
default=http://localhost:3000/api/v1
