export PORT=3000
export MONGO_URI=mongodb://localhost:27017/logs
export LOG_INGESTOR_URL=http://localhost:3000/api/v1

all: run

mongo:
	docker ps -a | grep -q mongodb && (docker ps | grep -q mongodb || docker container start mongodb) || docker run --name mongodb -p 27017:27017 -d mongodb/mongodb-community-server:latest

build:
	npm --prefix frontend install
	npm --prefix frontend run build
	go build -o bin/ingestor cmd/ingestor/main.go
	go build -o bin/generator cmd/generator/main.go

run:
	docker compose up

dev: mongo
	DEBUG=true go run cmd/ingestor/main.go

frontend:
	npm --prefix frontend run dev

generate:
	go run cmd/generator/main.go

populate:
	chmod +x ./scripts/populate.sh
	./scripts/populate.sh

.PHONY: mongo build run dev frontend generate populate
.SILENT: mongo build run dev frontend generate populate
