package main

import (
	"context"
	"embed"
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"gitlab.com/maeldonn/ingestor/binder"
	"gitlab.com/maeldonn/ingestor/config"
	"gitlab.com/maeldonn/ingestor/database"
	"gitlab.com/maeldonn/ingestor/handlers"
)

//go:embed dist
var ui embed.FS

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Fatalf("Cannot load config: %v", err)
	}

	mongo, err := database.Connect(cfg.MongoURI)
	if err != nil {
		log.Fatalf("Cannot connect to mongo: %v", err)
	}
	defer mongo.Disconnect(context.Background())

	h := handlers.NewLog(mongo)

	e := setupEcho(cfg)
	e.HEAD("/api/v1", h.Ping)
	e.GET("/api/v1", h.FindLogs)
	e.POST("/api/v1", h.AddLog)

	if !cfg.Debug {
		e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
			Root:       "dist",
			HTML5:      true,
			Filesystem: http.FS(ui),
		}))
	}

	port := fmt.Sprintf(":%d", cfg.Port)
	if err := e.Start(port); err != nil {
		e.Logger.Fatal(err)
	}
}

// setupEcho return the echo instance configured
func setupEcho(cfg *config.Config) *echo.Echo {
	e := echo.New()

	e.Use(middleware.Recover())
	e.Use(middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
		LogStatus:  true,
		LogURI:     true,
		LogMethod:  true,
		LogLatency: true,
		LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
			fmt.Printf("%s %s %d %v\n", v.Method, v.URI, v.Status, v.Latency)
			return nil
		},
	}))

	e.Binder = binder.New()

	if cfg.Debug {
		e.Use(middleware.CORS())
		e.Logger.SetLevel(log.Lvl(log.DEBUG))
	}

	return e
}
