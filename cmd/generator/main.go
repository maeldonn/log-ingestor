package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"log/slog"
	"math/rand"
	"net/http"
	"os"
	"time"

	"gitlab.com/maeldonn/ingestor/types"
)

var (
	client    = &http.Client{Timeout: 100 * time.Millisecond}
	LogLevels = []string{
		"info",
		"debug",
		"warning",
		"fatal",
		"error",
	}
)

func main() {
	url := os.Getenv("LOG_INGESTOR_URL")
	if url == "" {
		log.Fatal("Please specify the LOG_INGESTOR_ADDR environement variable")
	}

	if _, err := Ping(url); err != nil {
		log.Fatalf("Log ingestor server is not available: %v", err)
	}

	ticker := time.NewTicker(500 * time.Millisecond)
	for range ticker.C {
		if err := SendLog(url); err != nil {
			slog.Error("Failed to send log", slog.String("error", err.Error()))
		}
	}
}

// Ping return an error if the log ingestor server isn't available
func Ping(url string) (int, error) {
	req, err := http.NewRequest(http.MethodHead, url, nil)
	if err != nil {
		return 0, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return 0, err
	}

	return resp.StatusCode, nil
}

// SendLog create a log in the log ingestor server
func SendLog(url string) error {
	in := types.LogCreateInput{
		Level:      LogLevels[rand.Intn(len(LogLevels))],
		Message:    "This is a fake log",
		ResourceID: fmt.Sprintf("server-%d", rand.Intn(9000)+1000),
		Timestamp:  time.Now(),
		TraceID:    fmt.Sprintf("trace-%d", rand.Intn(900)+100),
		SpanID:     fmt.Sprintf("span-%d", rand.Intn(900)+100),
		Commit:     fmt.Sprintf("%d", rand.Intn(9000000)+1000000),
		Metadata: types.Metadata{
			ParentResourceID: fmt.Sprintf("server-%d", rand.Intn(9000)+1000),
		},
	}

	b, err := json.Marshal(&in)
	if err != nil {
		return err
	}

	resp, err := client.Post(url, "application/json", bytes.NewReader(b))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("bad status code, received %d", resp.StatusCode)
	}

	return nil
}
